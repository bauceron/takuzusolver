package sat_solver;

import java.util.ArrayList;
import java.util.List;

/**
 * The class is used like a stack
 * 
 * Class to save the state the differents state of the dpll if we need to choose
 * another "branch" and then unstack
 * 
 * @author Theo Rey-Viviant / Corentin Lumineau / Pierre-Elie Dard
 */
public class Stack {
    private List<Pack> packList;

    /**
     * Initialize a new stack
     */
    public Stack() {
        this.packList = new ArrayList<Pack>();
    }

    /**
     * Remove the top of the stack
     */
    public void unstack() {
        this.packList.remove(this.getTop());
    }

    /**
     * Check if the stack is empty
     * 
     * @return stack is empty
     */
    public Boolean isEmpty() {
        return this.packList.isEmpty();
    }

    /**
     * Add a new element to the stack
     * 
     * We need to recreate the object of type VecClause and Vars to deeply copy the
     * object
     * 
     * @param vecClause vecClause
     * @param vars      variable list
     * @param var       variable
     * @param value     value of the variable
     */
    public void add(VecClause vecClause, int[] vars, int var, int value) {
        VecClause newVec = new VecClause();
        for (int i = 0; i < vecClause.getSize(); i++)
            newVec.add(new Clause(vecClause.get(i).toArray()));

        int[] newVars = new int[vars.length];
        for (int i = 0; i < vars.length; i++)
            newVars[i] = vars[i];

        Pack clonedPack = new Pack(newVec, newVars, var, value);
        this.packList.add(clonedPack);
    }

    /**
     * Return the position of the top of the stack
     * 
     * @return top of the stack
     */
    public int getTop() {
        return this.packList.size() - 1;
    }

    /**
     * Return the size of the stack
     * 
     * @return size
     */
    public int getSize() {
        return this.packList.size();
    }

    /**
     * Set the value in the pack for the top pack of the stack
     * 
     * @param value value to insert
     */
    public void setTopValue(int value) {
        if (packList.size() > 0) {
            Pack local = this.packList.get(this.packList.size() - 1);
            local.setValue(value);
            this.packList.set(this.packList.size() - 1, local);
        }
    }

    /**
     * Set the value in the pack for index
     * 
     * @param value value to insert
     * @param pos   index
     */
    public void setValue(int value, int pos) {
        if (packList.size() >= pos) {
            Pack local = this.packList.get(pos);
            local.setValue(value);
            this.packList.set(pos, local);
        }
    }

    /**
     * Return the VecClause in the pack for the given index of the stack
     * 
     * @param i index
     * @return VecClause
     */
    public VecClause getVec(int i) {
        return this.packList.get(i).vecClause;
    }

    /**
     * Return the VecClause in the pack from the top pack in the stack
     * 
     * @return VecClause
     */
    public VecClause getTopVec() {
        return this.packList.get(this.packList.size() - 1).vecClause;
    }

    /**
     * Return the Value in the pack for the given index of the stack
     * 
     * @param i index
     * @return value
     */
    public int getValue(int i) {
        return this.packList.get(i).value;
    }

    /**
     * Return the Value in the pack from the top pack in the stack
     * 
     * @return Value
     */
    public int getTopValue() {
        return this.packList.get(this.packList.size() - 1).value;
    }

    /**
     * Return the Variable in the pack for the given index of the stack
     * 
     * @param i index
     * @return variable
     */
    public int getVar(int i) {
        return this.packList.get(i).var;
    }

    /**
     * Return the Variable in the pack from the top pack in the stack
     * 
     * @return variable
     */
    public int getTopVar() {
        return this.packList.get(this.packList.size() - 1).var;
    }

    /**
     * Return the Vars in the pack for the given index of the stack
     * 
     * @param i index
     * @return int array
     */
    public int[] getVars(int i) {
        return this.packList.get(i).vars;
    }

    /**
     * Return the Vars in the pack from the top pack in the stack
     * 
     * @return int array
     */
    public int[] getTopVars() {
        return this.packList.get(this.packList.size() - 1).vars;
    }

    /**
     * Class to symplify the construction of the pack
     */
    class Pack {
        public VecClause vecClause;
        public int[] vars;
        public int var;
        public int value;

        /**
         * Constructor for the pack
         * 
         * @param vecClause
         * @param vars
         * @param var
         * @param value
         */
        public Pack(VecClause vecClause, int[] vars, int var, int value) {
            this.vecClause = vecClause;
            this.vars = vars;
            this.var = var;
            this.value = value;
        }

        /**
         * Change the value in the pack
         * 
         * @param value
         */
        public void setValue(int value) {
            this.value = value;
        }

    }
}