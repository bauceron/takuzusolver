package sat_solver;

import java.util.ArrayList;
import java.util.List;
import org.sat4j.specs.*;



/**
 * Class to simplify the use of the clause's vector
 * 
 * @author Theo Rey-Viviant / Corentin Lumineau / Pierre-Elie Dard
 */

public class VecClause {
    private List<Clause> currVec;

    /**
     * Initialize a new vector
     */
    public VecClause() {
        currVec = new ArrayList<Clause>();
    }

    /**
     * Initialize a new vector From IVec<IVecInt>
     * 
     * @param iVec
     */

    public VecClause(IVec<IVecInt> iVec) {
        this.currVec = new ArrayList<Clause>();
        for(int i = 0; i<iVec.size(); i++)
            this.currVec.add(new Clause(iVec.get(i).toArray()));
    }

    /**
     * Add a clause to the vector
     * 
     * @param clause
     */
    public void add(Clause clause) {
        currVec.add(clause);
    }

    /**
     * Return the size of the vector
     * 
     * @return size
     */
    public int getSize() {
        return currVec.size();
    }

    /**
     * Return the clause at the index i of the vector
     * 
     * @param i index
     * @return clause
     */
    public Clause get(int i) {
        return currVec.get(i);
    }
}