package sat_solver;

import java.util.List;
import java.util.ArrayList;

/**
 * Class to run the whole sat solver in n-sat
 * 
 * @author Theo Rey-Viviant / Corentin Lumineau / Pierre-Elie Dard
 */
public class Solver {
    private int[] vars;
    private int nbClause;
    public VecClause vecClause;
    private Boolean isSatifaisable; // Global satisfiability

    private int[] result;

    // Only for the dpll
    private Boolean currIsSatisfaisable; // Local satisfiability used to depile
    private Stack savedState;

    /**
     * Initialize a new solver with the needed parameters
     * 
     * @param nbVar     number of var
     * @param nbClause  number of clause
     * @param vecClause vector of clause
     */
    public Solver(int nbVar, int nbClause, VecClause vecClause) {
        this.vars = new int[nbVar + 1];
        this.result = new int[nbVar];
        this.vecClause = vecClause;
        this.nbClause = nbClause;
        this.savedState = new Stack();
        this.currIsSatisfaisable = true;
        this.isSatifaisable = true;

        for (int i = 0; i < vars.length; i++) // Initialize vars
            vars[i] = -1;

        this.dpll(); // Do the dpll
    }

    /**
     * Return if there is a solution for the current sat solver
     * 
     * @return solvability
     */
    public boolean isSatisfiable() {
        return this.isSatifaisable;
    }

    /**
     * Return a model in an int array shape for the current sat
     * 
     * @return model
     */
    public int[] model() {
        return this.result;
    }

    /**
     * Reduction according to the dpll algorithm
     * 
     * @return if at least a clause has been modified or removed
     */
    private boolean reduction() {
        boolean verif = false;
        for (int i = 0; i < this.vecClause.getSize() - 1; i++) // Parse all the clause
            if (this.vecClause.get(i).exist())
                for (int y = i + 1; y < this.vecClause.getSize(); y++) { // Parse all the other clause and verify if
                                                                         // they contain each other
                    if (this.vecClause.get(y).exist()) {
                        if (isInside(this.vecClause.get(i).toArray(), this.vecClause.get(y).toArray())) { // If
                                                                                                          // clause
                                                                                                          // y
                                                                                                          // contain
                                                                                                          // clause
                                                                                                          // i
                            this.vecClause.get(y).remove();
                            verif = true;
                        } else if (isInside(this.vecClause.get(y).toArray(), this.vecClause.get(i).toArray())) { // If
                                                                                                                 // clause
                                                                                                                 // i
                                                                                                                 // contain
                                                                                                                 // clause
                                                                                                                 // y
                            this.vecClause.get(i).remove();
                            verif = true;
                        }
                    }
                }
        return verif;
    }

    /**
     * Isolate literals according to the dpll algorithm
     * 
     * @return if at least a clause has been modified or removed
     */
    private boolean isolateLiterals() {
        boolean verif = false;
        boolean finish = false; // Value to remove the clause recursivly
        int state = 0;
        int y;
        List<Integer> list;
        while (!finish) { // If we already removed all the isoled var
            finish = true;
            for (int i = 1; i < vars.length; i++) { // Parse all the vars
                y = 0;
                list = new ArrayList<Integer>();
                state = 0;
                for (; y < this.vecClause.getSize(); y++) { // Verify if this var always have the same value
                    if (this.vecClause.get(y).exist())
                        if (contain(this.vecClause.get(y).toArray(), i)
                                && contain(this.vecClause.get(y).toArray(), (-i))) { // If we contain a and -a in the
                                                                                     // same clause
                            this.currIsSatisfaisable = false;
                            return false;
                        } else if (contain(this.vecClause.get(y).toArray(), i)) { // If we contain a
                            if (state == 0 || state == 1) {
                                state = 1;
                                list.add(y);
                            } else
                                break;
                        } else if (contain(this.vecClause.get(y).toArray(), (-i))) {// If we contain -a
                            if (state == 0 || state == 2) {
                                state = 2;
                                list.add(y);
                            } else
                                break;
                        }
                }

                if (y >= this.vecClause.getSize() && state != 0) { // If this var always have the same value delete all
                                                                   // the
                                                                   // clause that contain and set the var
                    finish = false; // They can be other vars to remove so we doing it again
                    verif = true;
                    for (int j = 0; j < list.size(); j++) {
                        this.vecClause.get(list.get(j)).remove();
                    }
                    if (state == 1) { // Write the right value for the var
                        this.vars[i] = 1;
                    } else if (state == 2) {
                        this.vars[i] = 0;
                    }
                }
            }
        }
        return verif;
    }

    /**
     * Unitary resolution isoltae literals to the dpll algorithm
     * 
     * @return if at least a clause has been modified or removed
     */
    private boolean unitaryResolution() {
        boolean modified = false;
        for (int i = 0; i < this.vecClause.getSize(); i++) { // Parsing all the clause
            if (this.vecClause.get(i).exist() && this.vecClause.get(i).getLength() == 1) {
                int varUnit = this.vecClause.get(i).toArray()[0];
                if (varUnit < this.vars.length) {
                    if (!removeVar(varUnit))
                        return false;
                    if (varUnit > 0) {
                        this.vars[Math.abs(varUnit)] = 1;
                    } else {
                        this.vars[Math.abs(varUnit)] = 0;
                    }
                    modified = true;
                }
            }
        }
        return modified;
    }

    /**
     * Remove the specified var in the VecClause
     * 
     * @param var to remove
     * @return if at least a clause has been modified or removed
     */
    private boolean removeVar(int var) {
        if (var < this.vars.length)
            for (int i = 0; i < this.vecClause.getSize(); i++) { // PArcours de toutes les clauses
                if (this.vecClause.get(i).getLength() == 1 && this.vecClause.get(i).toArray()[0] == -var) { // test si
                                                                                                            // il y
                                                                                                            // a "a" et
                                                                                                            // "-a"
                    this.currIsSatisfaisable = false;
                    return false;
                } else if (contain(this.vecClause.get(i).toArray(), var)) { // si la clause contient la var
                                                                            // à suppr
                    this.vecClause.get(i).remove(); // suppression de la clause
                } else if (contain(this.vecClause.get(i).toArray(), -var)) { // si la clause contient le
                                                                             // complémentaire de var
                    int[] newClause = new int[this.vecClause.get(i).getLength() - 1]; // suppression de ce litteral dans
                                                                                      // la
                                                                                      // clause
                    int n = 0;
                    for (int y = 0; y < this.vecClause.get(i).getLength() && n < newClause.length; y++) {
                        if (this.vecClause.get(i).toArray()[y] != -var) {
                            newClause[n] = this.vecClause.get(i).toArray()[y];
                            n++;
                        }
                    }
                    this.vecClause.get(i).setArray(newClause);
                }
            }
        return true;
    }

    /**
     * Run the whole dpll and fill the result file if it's satisfiable
     * 
     * According striclty to the dpll original algorithm
     */
    private void dpll() {
        int varToChoose = 0;
        while (this.isSatifaisable && !this.isEmpty()) {
            this.currIsSatisfaisable = true;
            if (!this.reduction() && !this.isolateLiterals())
                if (this.currIsSatisfaisable) {
                    if (!this.unitaryResolution())
                        if (this.currIsSatisfaisable) {
                            varToChoose = this.bestVar();
                            if (varToChoose != 0) { // Stack
                                // System.out.println("Stack");
                                savedState.add(this.vecClause, this.vars, varToChoose, 1);
                                this.vars[varToChoose] = 1;
                                if (!this.removeVar(varToChoose))
                                    this.unstack();
                            } else { // Unstack
                                this.unstack();
                            }
                        } else { // Unstack
                            this.unstack();
                        }
                } else { // Unstack
                    this.unstack();
                }
        }
        if (this.isSatifaisable) {
            for (int i = 1; i < this.vars.length; i++) {
                this.result[i - 1] = (this.vars[i] > 0) ? i : -i;
            }
        }
    }

    /**
     * Verify if the VecClause isn't empty (if some clause are still active)
     * 
     * @return isempty
     */
    private boolean isEmpty() {
        for (int i = 0; i < this.vecClause.getSize(); i++)
            if (this.vecClause.get(i).exist())
                return false;
        return true;
    }

    /**
     * Unstack the pile and get the right state to try another variable in the dpll
     */
    private void unstack() {
        if (!savedState.isEmpty() && savedState.getTopValue() == 1) { // If we haven't tested the other value for this
                                                                      // var
            // System.out.println("Change top value");
            this.vecClause = savedState.getTopVec();
            this.vars = savedState.getTopVars();
            this.vars[savedState.getTopVar()] = 0;
            savedState.setTopValue(0);
            if (!this.removeVar(-savedState.getTopVar()))
                this.unstack();
        } else if ((savedState.getSize() > 1) && savedState.getTopValue() == 0) { // If we need to go up in the stack
            // System.out.println("Unstack");
            savedState.unstack();
            this.vecClause = savedState.getTopVec();
            this.vars = savedState.getTopVars();
            this.unstack();
        } else {
            this.isSatifaisable = false;
        }
    }

    /**
     * Choose the bestVar to choose a value on for the DPLLL
     * 
     * @return best var to choose
     */
    private int bestVar() {
        int[] occVar = new int[this.vars.length];
        int max = 0;
        for (int i = 0; i < this.vecClause.getSize(); i++)
            if (this.vecClause.get(i).exist())
                for (int y = 0; y < this.vecClause.get(i).toArray().length; y++)
                    if (vars[Math.abs(this.vecClause.get(i).toArray()[y])] == -1)
                        occVar[Math.abs(this.vecClause.get(i).toArray()[y])]++;
        for (int i = 1; i < occVar.length; i++) {
            if (occVar[i] > occVar[max]) {
                max = i;
            }
        }
        // System.out.println(max + " / " + occVar[max]);
        return max;
    }

    /**
     * Return the value of the assertion "array contain the value key"
     * 
     * @param array
     * @param key
     * @return contain
     */
    private static boolean contain(int[] array, int key) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key)
                return true;
        }
        return false;
    }

    /**
     * Return the value of the assertion "each element of array1 if inside array2"
     * 
     * @param array1
     * @param array2
     * @return is inside
     */
    private static boolean isInside(int[] array1, int[] array2) {
        for (int i = 0; i < array1.length; i++) {
            int j = 0;
            for (; j < array2.length; j++) {
                if (array1[i] == array2[j])
                    break;
            }
            if (j >= array2.length)
                return false;
        }
        return true;
    }
}