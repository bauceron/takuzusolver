package sat_solver;

/**
 * Class to represent a clause and simplify the utilisation of the clause into
 * the solver
 * 
 * A clause is internally an int array but can be 'desactivated' and 'activated'
 * for the needs
 * 
 * @author Theo Rey-Viviant / Corentin Lumineau / Pierre-Elie Dard
 */
public class Clause {
    private int[] clause;
    private boolean exist;

    /**
     * Initialise a new clause with the int array in parameter
     * 
     * On default the clause is activated
     * 
     * @param clause int array
     */
    public Clause(int[] clause) {
        this.clause = clause;
        this.exist = true;
    }

    /**
     * Return the value of a clause at a certain index
     * 
     * @param i index in the clause
     * @return value of the index
     */
    public Integer get(int i) {
        return new Integer(this.clause[i]);
    }

    /**
     * Return the int array of the clause
     * 
     * @return int array of clause
     */
    public int[] toArray() {
        return this.clause;
    }

    /**
     * Update the int array contain in the clause
     * 
     * @param newClause int array for the new clause
     */
    public void setArray(int[] newClause) {
        this.clause = newClause;
        this.exist = true;
    }

    /**
     * Return the length of the int array contain in the clause
     * 
     * @return length of the clause
     */
    public int getLength() {
        return this.clause.length;
    }

    /**
     * Reactivate the clause
     */
    public void add() {
        this.exist = true;
    }

    /**
     * Disable the clause
     */
    public void remove() {
        this.exist = false;
    }

    /**
     * Verify if the clause is activated
     * 
     * @return activation state
     */
    public boolean exist() {
        return this.exist;
    }
}