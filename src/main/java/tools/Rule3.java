package tools;

import org.sat4j.specs.*;
import java.lang.Integer;
import org.sat4j.core.*;

public class Rule3 extends Fichier {

	IVec<IVecInt> r3;
	int length;
	int nbVar;

	public Rule3(int length, int nbVar, IVec<IVecInt> currVec) {
		r3 = currVec;
		this.length = length;
		this.nbVar = nbVar;
	}

	public int getNbVar() {
		return this.nbVar;
	}
	// Création d'un vecteur contenant toutes les clauses relatives à la règle 2
    	// (2 lignes ou 2 colonnes ne peuvent être identiques)
	public IVec<IVecInt> ruleThree() { // n etant le nbr de cases par ligne ou colonne
		// nombre de variables : n*n;
		int i = 0;
		int j = 0;
		int n = this.length;
		while (i != n) { // ecriture des comparaisons entre colonnes
			j = 0;
			while (j != n) {
				if (i != j) {
					rule3CompCol(i, j, n); // rule3CompCol remplit le ArrayString avec les clauses correspondant à la
											// comparaison entre colonnes

				}
				j++;
			}
			i++;
		}
		i = 0; // ecriture des comparaisons entre lignes
		while (i != n) {
			j = 0;
			while (j != n) {
				if (i != j) {
					rule3CompLig(i, j, n);
				}
				j++;
			}
			i++;
		}
		return this.r3;
	}
	// comparaison entre les colonnes c1 et c2, pour un tableau de taille n*n
	public String rule3CompCol(int c1, int c2, int n1) {
		String s = "";
		String lebin = "";
		int i = 0;
		IIISAT s3 = new IIISAT(this.nbVar);
		while (i != n1) {	// création de la variable lebin, String composé de n 0
			lebin = lebin + "0";
			i++;
		}

		i = 0;
		c1 += 1;
		c2 += 1;
		int j = 0;
		int k = 0;
		while (k != n1) {	//chaque passage dans cette boucle écrit une clause selon la valeur "lebin", qui s'incrémente automatiquement
			i = 0;
			while (i != n1) { // écriture d'une clause
				j = 0;
				s = "";
				while (j != n1) { // avance dans la ligne
					if (lebin.charAt(j) == '0') { // écriture de 2 variables de la clause selon la valeur de lebin
						s += "-" + ((c1 + n1 * j)) + " -" + ((c2 + n1 * j)) + " ";
					} else {
						s += ((c1 + n1 * j)) + " " + ((c2 + n1 * j)) + " ";
					}
					j++;
				}
				this.addIVecArray(r3, s3.nSATTo3SAT(s));
				this.nbVar = s3.getNbVar();
				i++;
				lebin = incrBin(lebin);		// incrémentation du nombre binaire
			}
			k++;
		}
		return s;
	}
	// comparaison entre les lignes l1 et l2, pour un tableau de taille n*n
	public String rule3CompLig(int l1, int l2, int n1) {
		String s = "";
		String lebin = "";
		int i = 0;
		IIISAT s3 = new IIISAT(this.nbVar);
		while (i != n1) {	// création de la variable lebin, String composé de n 0
			lebin = lebin + "0";
			i++;
		}
		i = 0;
		int j = 0;
		int k = 0;
		while (k != n1) {	//chaque passage dans cette boucle écrit une clause selon la valeur "lebin", qui s'incrémente automatiquement
			i = 0;
			while (i != n1) { // écriture d'une clause
				j = 0;
				s = "";
				while (j != n1) { // avance dans la ligne
					if (lebin.charAt(j) == '0') {	// écriture de 2 variables de la clause selon la valeur de lebin
						s += "-" + ((l1 * n1 + j + 1)) + " -" +((l2) * n1 + j + 1) + " ";
					} else {
						s += ((l1 * n1 + j + 1)) + " " + ((l2 * n1 + j + 1)) + " ";
					}
					j++;
				}
				this.addIVecArray(r3, s3.nSATTo3SAT(s));
				this.nbVar = s3.getNbVar();
				i++;
				lebin = incrBin(lebin);		// incrémentation du nombre binaire
			}
			k++;
		}
		return s;
	}
	// permet d'incrémenter un String représentant un nombre binaire
	// exemple : incrBin("01001") renvoie "01010"
	public String incrBin(String sbin) {
		int i = 0;
		int c = 0;
		while (i != sbin.length()) {
			c = c * 2 + ((int) (sbin.charAt(i)) - (int) ('0'));
			i++;

		}
		c++;
		String s1 = "";
		s1 = "" + Integer.toBinaryString(c);
		int diff = sbin.length() - s1.length();
		i = 0;
		while (i < diff) {
			s1 = "0" + s1;
			i++;
		}
		return s1;
	}
}