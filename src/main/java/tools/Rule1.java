package tools;

import org.sat4j.specs.*;

public class Rule1 extends Fichier {

    public int length;

    public IVec<IVecInt> r1;
    public int nbVar;

    /**
     * Constructeur
     * 
     * @param length
     * @param nbVar
     * @param currVec
     */

    public Rule1(int length, int nbVar, IVec<IVecInt> currVec) {
        this.nbVar = nbVar;
        this.length = length;
        r1 = currVec;
    }

    /**
     * Methode getNbVar
     * 
     * renvoie le nombre de variable courante de la regle 1
     * 
     * @return
     */
    public int getNbVar() {
        return this.nbVar;
    }


    /**
     * Methode ruleOne()
     * 
     * Renvoie le vecteur de clauses représentant la règle n°1
     * 
     * @return
     */
    public IVec<IVecInt> ruleOne() {

        for (int i = 0; i < this.length; i++) { // Créé les clause de la regle n°1 sur les lignes
            for (int j = 1; j <= this.length - 2; j++) {
                r1.push(this.StringToVecInt(("-" + (i * this.length + j) + " -" + (i * this.length + j + 1) + " -"
                        + (i * this.length + j + 2))));

                r1.push(this.StringToVecInt(
                        (i * this.length + j) + " " + (i * this.length + j + 1) + " " + (i * this.length + j + 2)));
            }
        }

        for (int i = 0; i < this.length - 2; i++) { // Créé les clause de la regle n°1 sur les colones
            for (int j = 1; j <= this.length; j++) {
                this.r1.push(this.StringToVecInt("-" + (i * this.length + j) + " -" + ((i + 1) * this.length + j) + " -"
                        + ((i + 2) * this.length + j)));

                this.r1.push(this.StringToVecInt(
                        (i * this.length + j) + " " + ((i + 1) * this.length + j) + " " + ((i + 2) * this.length + j)));
            }
        }
        return this.r1;
    }
}