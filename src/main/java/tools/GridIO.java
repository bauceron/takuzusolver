package tools;

import org.sat4j.specs.*;
import org.sat4j.core.*;

/**
 * Class GridIO etendu à Fichier
 * 
 * Gère l'importation de la grille à résoudre pour en intégrer les clauses.
 * 
 */

public class GridIO extends Fichier {

    public int length;
    public IVec<IVecInt> gridCnf;

    /**
     * Constructeur GridIO() :
     * 
     * @param length  longueur de la grille
     * @param currVec Vecteur de clause étant soit vide soit contenant des clauses
     *                des autres regles
     */

    public GridIO(int length, IVec<IVecInt> currVec) {
        gridCnf = currVec;
        this.length = length;
    }

    /**
     * Methode gridImport() :
     * 
     * Methode lisant le document contenant la grille à remplir et le lit pour
     * mettre les valeur de la grille sous forme de clause
     * 
     * @param ndFichier
     * @return
     */

    public IVec<IVecInt> gridImport(String ndFichier) {
        this.demarrer(ndFichier);
        for (int i = 0; i < this.length; i++) {
            for (int j = 1; j <= this.length; j++) {
                if (this.elementCourant() != '.') {
                    if (this.elementCourant() == '1') {
                        int[] clause = new int[1];
                        clause[0] = i * this.length + j;
                        this.gridCnf.push(new VecInt(clause));
                    } else if (this.elementCourant() == '0') {
                        int[] clause = new int[1];
                        clause[0] = -(i * this.length + j);
                        this.gridCnf.push(new VecInt(clause));
                    }
                }
                this.avancer();
            }
        }
        this.close();

        return this.gridCnf;
    }
}