package tools;

import java.util.ArrayList;
import java.lang.Integer;
import org.sat4j.specs.*;
import org.sat4j.core.*;

public class IIISAT extends Fichier {
	public int nbVar;

	/**
	 * Construit l'objet IIISAT modifiant les clauses
	 * 
	 * @param nbVar prend le nombre de variable deja présente et ajoute à ce nombre
	 *              chaques variables rajoutées
	 */

	public IIISAT(int nbVar) {
		this.nbVar = nbVar;
	}

	/**
	 * Methode getNbVar()
	 * 
	 * Renvoie le nombre de variable de l'ojet courant
	 * 
	 * @return
	 */

	public int getNbVar() {
		return this.nbVar;
	}

	/**
	 * Methode clauseToArrayLitte
	 * 
	 * Renvoie la liste d'entier correspondant à la clause rentrée en parametre
	 * 
	 * @param clause clause sous forme de String à transformer
	 * @return
	 */

	public ArrayList<Integer> clauseToArrayLitte(String clause) {
		String currLitte = this.avancerMot(0, clause);
		ArrayList<Integer> vectClause = new ArrayList<Integer>();
		int i = 0;

		while (i < clause.length()) { // parcour les mots du string et les ajoute dans Arraylist les un aprres les
										// autres
			vectClause.add(Integer.parseInt(currLitte));
			i += currLitte.length() + 1;
			currLitte = this.avancerMot(i, clause);
		}
		return vectClause;
	}

	/**
	 * Methode nSATTo3SAT()
	 * 
	 * renvoie la version 3-SAT de la clause rentrée en paramètre
	 * 
	 * @param clause : clause nSat à passer en 3-SAT
	 * @return
	 */

	public IVec<IVecInt> nSATTo3SAT(String clause) {

		IVec<IVecInt> iiiSAT = new Vec<IVecInt>();
		ArrayList<Integer> nSAT = this.clauseToArrayLitte(clause);
		if (nSAT.size() > 3) { // entre que si la clause est de longueur supérieurs à 3 literaux
			for (int j = 0; j < nSAT.size(); j++) {
				if (j == 0) { 
					iiiSAT.push(this.StringToVecInt(nSAT.get(0) + " " + nSAT.get(1) + " " + (this.nbVar + 1)));
					j += 1;
				} else if (j < nSAT.size() - 2) {
					iiiSAT.push(
							this.StringToVecInt("-" + (this.nbVar + 1) + " " + nSAT.get(j) + " " + (this.nbVar + 2)));
					this.nbVar += 1;
				} else if (j == nSAT.size() - 2) {
					iiiSAT.push(
							this.StringToVecInt("-" + (this.nbVar + 1) + " " + nSAT.get(j) + " " + nSAT.get(j + 1)));
					this.nbVar += 1;

				}
			}
		} else {
			iiiSAT.push(this.StringToVecInt(nSAT.get(0) + " " + nSAT.get(1) + " " + nSAT.get(2)));
		}
		return iiiSAT;
	}
}