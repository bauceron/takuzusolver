package tools;

import java.io.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import org.sat4j.specs.*;
import org.sat4j.core.VecInt;

/**
 * Class Fichier
 * 
 * Class supportant de nombreux outils pour les autres class que ce soit la
 * lecture et l'ecriture de document ou chaine de caractaires mais aussi dans le
 * changement de type ces methodes sont ici mise car isolées mais dont
 * l'utilisation est importante de partout
 * 
 */

public class Fichier {

    private BufferedReader lect;
    public int varCourante;
    public char charCourant;
    public String ndFichier;
    private String separateurs = " _~&{}'#()[]-|`^@\\}/*-+=°?,;:§!µ%><’\r\n\0\t\"";

    /**
     * Methodes données avec le projet
     * 
     */

    static public void afficheFichier(String s) {
        String ligne;
        try {
            Reader r = new FileReader(s);
            BufferedReader br = new BufferedReader(r);
            while ((ligne = br.readLine()) != null) {
                System.out.println(ligne);
            }
            r.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    static public void ecrireFichier(String f, String s) {
        try {
            Writer w = new FileWriter(f, true);

            BufferedWriter output = new BufferedWriter(w);
            output.write(s);
            output.flush();
            output.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    static public void creerFichier(String f, String s) {
        try {
            System.out.println("debug");
            Writer w = new FileWriter(f);
            BufferedWriter output = new BufferedWriter(w);
            output.write(s);
            output.flush();
            output.close();
            System.out.println("debug");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Methode demarrer() Initialise la lecture du document et avance sur le premier
     * mot
     * 
     * @param ndFichier nom du fichier à initialiser
     */

    public void demarrer(String ndFichier) {
        try {
            this.lect = new BufferedReader(new InputStreamReader(new FileInputStream(ndFichier), "UTF8"));
            this.varCourante = 0;
        } catch (Exception exep) {
            System.out.println();
            System.out.println("Cannot read the file : " + this.ndFichier);
            System.out.println(exep);
        }
        this.avancer();
    }

    /**
     * Methode avancer()
     * 
     * Elle permet dans un d'avancer lors d'un acces sequentiel, elle est ici
     * configurer pour avancer d'une "case" a une autre dans la lecture du fichier
     * contenant une grille à remplir
     * 
     */

    public void avancer() {
        try {
            if (this.varCourante == 48 || this.varCourante == 49 || this.varCourante == 46) {
                this.varCourante = this.lect.read();
            }
            while (!this.finDeSequence() && this.estSeparateur((char) this.varCourante)) {
                this.varCourante = this.lect.read();
            }
        } catch (Exception exep) {
            System.out.println(exep);
        }
    }

    /**
     * Methode close()
     * 
     * Termine la lecture du document
     * 
     */

    public void close() {
        try {
            this.lect.close();
        } catch (Exception exep) {
            System.out.println(exep);
        }
    }

    /**
     * Methode estSeparateur()
     * 
     * Renvoie vrai si le caractère c fais partie des séparateurs dans les attributs
     * sinon faux
     * 
     * @param c caractaire à tester
     * @return
     */

    private boolean estSeparateur(char c) {
        /*
         * Faire une recheche de c dans la chaîne de(this.charCourant == 0) caracteres
         * separateurs.
         */
        boolean b = false;
        int curseur = 0;
        while (curseur != this.separateurs.length()) {
            b = b | (c == this.separateurs.charAt(curseur));
            curseur++;
        }
        return b; // renvoi d'une expression booleene vraie si le caractere c est dans l'attibut
                  // separateurs.FileInputStream
    }

    /**
     * Methode finDeSequence()
     * 
     * renvoie vrai si on arrive à la fin de séquence du fichier
     * 
     * @return
     */

    public boolean finDeSequence() {
        return (this.varCourante == -1);
        // renvoi d'une expression boolenne vraie si la fin du fichier est atteinte.

    }

    /**
     * Methode elementCourant()
     * 
     * renvoie la variable courante
     * 
     * @return
     */

    public int elementCourant() {
        return this.varCourante;
    }

    /**
     * Methode avancerMot()
     * 
     * Different de avancer(), avancerMot() lit le string s et reprend la lecture au
     * caractaire d'index i il renvoie le mot suivant le l'index i
     * 
     * @param i index de reprise
     * @param s String à lire
     * @return
     */

    public String avancerMot(int i, String s) {
        String motCourant = "";

        while (i < s.length() && s.charAt(i) != ' ') {
            motCourant = motCourant + s.charAt(i);
            i++;
        }
        return motCourant;
    }

    /**
     * Methode StringToVecInt()
     * 
     * Transforme une clause qui est sous forme de String IvecInt
     * 
     * @param clause : une clause sous forme de string
     * @return
     */

    public IVecInt StringToVecInt(String clause) {
        int j = 0;
        int[] iClause = new int[3];
        String currLitte = "";
        for (int i = 0; i < iClause.length; i++) {
            iClause[i] = Integer.parseInt(this.avancerMot(j, clause));
            currLitte = "" + iClause[i];
            j += currLitte.length() + 1;
        }
        IVecInt vec = new VecInt(iClause);
        return (vec);
    }

    /**
     * Methode addIVecArray()
     * 
     * Rajoute les éléments de a2 à a1
     * 
     * @param a1 liste de clause une
     * @param a2 liste de clause deux
     * @return
     */
    public IVec<IVecInt> addIVecArray(IVec<IVecInt> a1, IVec<IVecInt> a2) {
        for (int i = 0; i < a2.size(); i++) {
            a1.push(a2.get(i));
        }
        return a1;
    }

}