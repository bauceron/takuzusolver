package tools;

import org.sat4j.specs.*;

public class Rule2 extends Fichier {
    IVec<IVecInt> r2;
    int length;
    int nbVar;

    // Constructeur de la classe
    /**
     * 
     * @param length  Taille de la grille
     * @param nbVar   Nombre de variable
     * @param currVec Vecteur de clause �tant soit vide, soit contenant les clauses
     *                des autres r�gles
     */
    public Rule2(int length, int nbVar, IVec<IVecInt> currVec) {
        this.nbVar = nbVar;
        this.length = length;
        r2 = currVec;
    }

    public int getNbVar() {
        return this.nbVar;
    }

    // Cr�ation d'un vecteur contenant toutes les clauses relatives � la r�gle 2
    // (Autant de 1 que de 0 dans les lignes et colonnes)
    public IVec<IVecInt> ruleTwo() {
        int n = this.length;
        int k = 0;
        while (k < n) { // Pour toutes les lignes
            int i = 1;
            int[] arr = new int[n]; // Creation d'un tableau de taille n
            while (i <= n) {
                arr[i - 1] = i + k * n; // Remplissage du tableau par les elements de la ligne
                i++;
            }
            int r = n / 2 + 1;
            this.printCombination(arr, n, r); // Cr�ation des combinaison r parmis n
            k++;

        }
        k = 1;
        while (k <= n) { // M�me principe, mais pour les colonnes
            int i = 0;
            int[] arr = new int[n];
            while (i < n) {
                arr[i] = k + i * n;
                i++;
            }
            int r = n / 2 + 1;
            this.printCombination(arr, n, r);
            k++;

        }
        return this.r2; // Retourne le vecteur comprenant toutes les clauses de la r�gle
    }

    // M�thode qui �crit toutes les combinaison de r parmis n demand�,grace au
    // tableau data cr�er dans printCombination , en 3-SAT dans un vecteur
    /**
     * 
     * @param arr   Tableau contenant les �l�ments a mettre en combinaison (ici
     *              �l�ment d'une ligne ou colonne)
     * @param n     Nombre d'�lement de arr (taille de la ligne/colonne)
     * @param r     Nombre d'�l�ment de la combinaison (r parmis n)
     * @param index Num�ro de l'�l�ment courant de arr (mettre 0,se met � jour dans
     *              la m�thode)
     * @param data  Tableau contenant la combinaison
     * @param i     Num�ro de l'�l�ment courant de data (mettre 0,se met � jour dans
     *              la m�thode)
     */
    public void combinationUtil(int arr[], int n, int r, int index, int data[], int i) {
        String temp = ""; // Cr�ation d'un string temporaire
        IIISAT s3 = new IIISAT(this.nbVar); // Appelle de la classe IIISAT pour passer en 3-SAT
        if (index == r) { // Si Le tableau temporaire est plein la combinaison est prete
            for (int j = 0; j < r; j++) {

                temp = temp + data[j] + " "; // On ecrit la combinaison
            }
            this.addIVecArray(r2, s3.nSATTo3SAT(temp)); // On la passe en 3-SAT
            this.nbVar = s3.getNbVar(); // On actualise le nombre de clause
            temp = ""; // On vide le string temporaire

            for (int j = 0; j < r; j++) { // Meme principe mais on ajoute la negation devant chaque litt�raux
                temp = temp + "-" + data[j] + " ";
            }
            this.addIVecArray(r2, s3.nSATTo3SAT(temp));
            this.nbVar = s3.getNbVar();
            temp = "";

            return;
        }

        // Quand il n'y a plus d'�l�ment a mettre dans data et que le tableau n'est pas
        // plein
        if (i >= n)
            return;

        // L'�l�ment courant est ajout�, on passe a l'�l�ment suivant dans data et dans
        // arr
        data[index] = arr[i];
        this.combinationUtil(arr, n, r, index + 1, data, i + 1);

        // L'�l�ment courant n'est pas ajout�, on passe a l'�l�ment suivant dans arr
        // mais pas dans data
        this.combinationUtil(arr, n, r, index, data, i + 1);
    }

    // M�thode qui demande la cr�ation de toutes les combinaisons de r parmis n
    /**
     * 
     * @param arr Tableau contenant les �l�ments a mettre en combinaison (ici
     *            �l�ment d'une ligne ou colonne)
     * @param n   Nombre d'�lement de arr (taille de la ligne/colonne)
     * @param r   Nombre d'�l�ment de la combinaison (r parmis n)
     */
    public void printCombination(int arr[], int n, int r) {
        int data[] = new int[r]; // Cr�ation d'un tableau de taille r qui stockera les combinaisons
                                 // temporairement

        this.combinationUtil(arr, n, r, 0, data, 0); // Cr�ation des combinaisons
    }

}
