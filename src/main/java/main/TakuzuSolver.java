package main;

import tools.*;
import java.lang.Integer;
import org.sat4j.specs.*;
import org.sat4j.core.*;
import org.sat4j.minisat.SolverFactory;
import sat_solver.*;

/*
 * ==============================================================
 * 
 *	Class TakuzuSolver : Reunie la construction des clause et lance les solvers
 * 
 * @nbClauses : Nombre de Clauses
 * @nbvar : Nombre de Littéraux.
 * @grid[][] : Grille acceuillant les reponses à afficher 
 * @length : taille de la grille
 * @Rule : Liste de Clauses
 * @ndFichier : Nom du fichier
 * 
 * 
 * =============================================================
 *
 */

class TakuzuSolver extends Fichier {

	public int nbClauses;
	public int nbVar;
	public int grid[][]; // grille représentant le jeu
	public int length;
	public IVec<IVecInt> rules;
	public String ndFichier = "test.cnf";
	public Boolean isSatisfesable;

	/**
	 * Constructeur Takuzusoler
	 * 
	 * @param length : Prend la longueur de la grille
	 */

	public TakuzuSolver(int length) {
		this.grid = new int[length][length];
		for (int i = 0; i < length; i++) { // remplissage de la Grille, necessaire pour la lecture de la grille de base
			for (int j = 0; j < length; j++) {
				this.grid[i][j] = (int) '.';
			}
		}
		this.ndFichier = "grids\\" + "grid" + length + "x" + length + ".txt";
		this.nbClauses = 0;
		this.nbVar = length * length;
		this.rules = new Vec<IVecInt>();
		this.isSatisfesable = false;
		this.length = length;

	}

	/**
	 * Methode rules() Additionne les clauses des différentes regles du jeu.
	 */

	public void rules() {
		long startTime = System.currentTimeMillis();
		System.out.println("Rule ONE : begin");
		///////////////////////// Génération des clauses de la règle 1
		Rule1 r1 = new Rule1(this.length, this.nbVar, rules);
		this.rules = r1.ruleOne();
		this.nbVar = r1.getNbVar();
		System.out.print(this.nbVar);
		/////////////////////////
		System.out.println("Execution : " + ((System.currentTimeMillis() - startTime)) + "ms");
		System.out.println("Rule ONE : Done");

		startTime = System.currentTimeMillis();
		System.out.println("Rule TWO : begin");
		///////////////////////// Génération des clause de la regle 2 à la suite de la
		///////////////////////// règle 1
		Rule2 r2 = new Rule2(this.length, this.nbVar, rules);
		this.rules = r2.ruleTwo();
		this.nbVar += r2.getNbVar();
		/////////////////////////
		System.out.println("Execution : " + ((System.currentTimeMillis() - startTime)) + "ms");
		System.out.println("Rule TWO : Done");

		startTime = System.currentTimeMillis();
		System.out.println("Rule THREE : begin");
		///////////////////////// Génération des clause de la regle 3 à la suite de la
		///////////////////////// règle 1 et 2
		Rule3 r3 = new Rule3(this.length, this.nbVar, rules);
		this.rules = r3.ruleThree();
		this.nbVar += r3.getNbVar();
		/////////////////////////
		System.out.println("Execution : " + ((System.currentTimeMillis() - startTime)) + "ms");
		System.out.println("Rule THREE : Done");

		startTime = System.currentTimeMillis();
		System.out.println("Grid IMPORT : begin");
		///////////////////////// Génération des clause de la Grille à remplir à la
		///////////////////////// suite de la règle 1, 2 et 3
		GridIO g = new GridIO(this.length, rules);
		this.rules = g.gridImport(this.ndFichier);
		/////////////////////////
		System.out.println("Execution : " + ((System.currentTimeMillis() - startTime)) + "ms");
		System.out.println("Grid IMPORT : Done");

		this.nbClauses = rules.size();// Nombre de clause totale
	}

	/**
	 * Methode Solver() : Rentre en parametre les clauses, le nombre de clause et le
	 * nombre de variable pour recuperer le modele et ainsi afficher la reponse
	 */

	public void solver() {

		Boolean v2 = false; // choix oui ou non pour utiliser notre version du solver

		long startTime = System.currentTimeMillis();
		System.out.println("Résolution...");

		if (v2 == false) {
			// initialisation du solveur
			ISolver solver = SolverFactory.newDefault();
			solver.newVar(this.nbVar);
			solver.setExpectedNumberOfClauses(this.nbClauses);
			try {
				// ajout des clause
				solver.addAllClauses(this.rules);
				IProblem problem = solver;

				if (problem.isSatisfiable()) {
					this.isSatisfesable = true;
					// recupération du model
					int[] model = problem.model();
					for (int i = 0; i < this.length; i++) {// remplissage de la grille
						for (int j = 0; j < this.length; j++) {
							if (model[i * this.length + j] > 0) {
								grid[i][j] = 1;
							} else {
								grid[i][j] = 0;
							}

						}
					}
				} else {
					System.out.println("UNSATISFAISABLE");
				}
			} catch (Exception exp) {
				System.out.println(exp);
			}
		} else {
			Solver sol = new Solver(this.nbVar, this.nbClauses, (new VecClause(this.rules)));

			int[] model = sol.model();
			for (int i = 0; i < this.length; i++) { // remplissage de la grille
				for (int j = 0; j < this.length; j++) {
					if (model[i * this.length + j] > 0) {
						grid[i][j] = 1;
					} else {
						grid[i][j] = 0;
					}

				}
			}
		}
		System.out.println("Execution : " + ((System.currentTimeMillis() - startTime)) + "ms");
		System.out.println("Resolution : DONE");
	}

	/**
	 * Methode setLength : Change l'attribut length pour pouvoir recuper l'argument
	 * du main et ainsi choisir la taille de la grille que l'on va resoudre
	 * 
	 * @param length
	 */

	public void setLength(int length) {
		this.length = length;
	}

	public void vecToCnf() {

		Boolean doIt = false;
		if (doIt == true) {
			String takuzuSolverHead = "c TakuzuSolver.cnf" + "\n" + "p cnf " + this.nbVar + " " + this.nbClauses + "\n";
			String temp = "";
			System.out.println(takuzuSolverHead);
			int[] clause;
			creerFichier("test.cnf", takuzuSolverHead);

			for (int i = 0; i < this.rules.size(); i++) {
				clause = this.rules.get(i).toArray();
				for (int j = 0; j < clause.length; j++) {
					if (temp == "") {
						temp = temp + clause[j];
					} else {
						temp = temp + " " + clause[j];
					}

				}
				temp = temp + " 0" + "\n";
				// System.out.println(temp);
				ecrireFichier("test.cnf", temp);
				temp = "";
			}
		}
	}

	public static void main(String[] args) throws Exception {

		int length = Integer.parseInt(args[0]);
		TakuzuSolver ts = new TakuzuSolver(length);
		ts.rules();
		ts.solver();

		System.out.println(" ");

		if (ts.isSatisfesable) {
			for (int i = 1; i <= length; i++) { // Affichage de la grille
				for (int j = 1; j <= length; j++) {
					System.out.print(" | ");
					System.out.print(ts.grid[i - 1][j - 1]);

				}
				System.out.print("\n");
			}
		}

		ts.vecToCnf();
	}
}
